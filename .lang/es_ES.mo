��    9      �  O   �      �  
   �  !   �       <        [     d  	   {     �     �     �     �     �     �     �     �     �                         4     L     Q     W     c  $   r     �     �     �     �     �     �     �     �  
                  4     ;     V     n     �     �     �     �     �  
   �     �     �  {   �  !   c     �     �     �  (   �     �  �   �     �	     �	     �	  Q   �	     8
     A
     Y
     l
     
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	             &     A     G     O     ]  0   n     �     �  "   �     �  	   �                    -  	   9  ,   C     p  )   y     �     �     �     �               %     9     H     M  ~   S  (   �     �          	  1   #     U              /                                     %   6      +   0       *       #       7              (           &       8             ,      5      4   1   )                        -      $   3      
   !   .               	   '             9      2                     "                 Colors  already exist. Type another name  theme? A Controls collection for change look of gambas applications Advanced Advanced Configuration All files Background  Pictures Background: Bars and Buttons Cancel Choose a file Clone the Theme  Close Colorize Icon's  Create a new Theme Default Delete Delete  Delete Selected Theme Duplicate selected tema Exit Font: Foreground: Free Selection Icons set according to Desktop Theme Image Files Imput Fields Invert Icons' Colors Maximize/Restore Minimize Name:  OK Picture to control size Properties Property Reset to Desktop Theme's icons Revert Revert to default settings Round Control's Radius: Round Panel's Radius: Save Changes Save Changes Made Save Settings Select a color System Colors Text Color Theme  Theme: Themes are not installed on this computer. You must install or run the gbThemes program in order to use this Theme control. Themes editor for gambas projects Título Value Visualization Example gbThemes: Gambas3 Projects Themes Editor line:  Project-Id-Version: gbThemes 3.18.3
PO-Revision-Date: 2023-07-10 07:05 UTC
Last-Translator: jorge <jorge@abu>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
     Colores  ya existe. Escriba otro nombre   tema? Una colección de controles para cambiar el aspecto de las aplicaciones de Gambas Avanzado Configuración avanzada Todos los archivos Imágenes de fondo Fondo: Barras y Botones Cancelar Escoge un archivo Clonar el tema Cerrar Colorear iconos Crear un Tema Nuevo Por defecto Eliminar Eliminar  Eliminar tema seleccionado Duplicar tema seleccionado Salir Fuente: Primer plano: Selección Libre Iconos configurados según el tema de escritorio Archivos de imagen Campos de entrada Invertir los colores de los iconos Maximizar/Restaurar Minimizar Nombre: OK Imagen a tamaño del control Propiedades Propiedad Restablecer los iconos de Tema de escritorio Revertir Volver a la configuración predeterminada Radio de control redondo: Radio del panel redondo: Guardar cambios Guardar cambios realizados Guardar ajustes Selecciona un color Colores del sistema Color de texto Tema Tema: Los temas no están instalados en esta computadora. Debe instalar o ejecutar el programa gbThemes para usar este control Tema. Editor de temas para proyectos de gambas título Valor Ejemplo de Visualización gbThemes: Editor de temas de proyectos de Gambas3 línea: 