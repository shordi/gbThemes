��    ;      �  O   �        
   	  !        6  <   >     {     �  	   �     �     �     �     �     �     �     �                    0     8     ?     G     ]     u     z     �     �  $   �     �     �     �     �     �                 
   *     5     >     D     c     j     �     �     �     �     �     �     �  
   �            {     !   �     �     �     �  (   �     	  �   	     �	  #   �	     
  J   
     h
     q
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
     	       	   (  	   2     <     U     q     x     ~     �  +   �     �     �     �               %     +  &   .  
   U  
   `  
   k  (   v  
   �  &   �     �     �          !     >     T     h     z     �     �  �   �  "   )     L     S     Z  -   u     �              '             1                        &   8      -   2       ,   !   $       9              *           (       :             .      7      6   3   +                        /      %   5      
   "   0              	   )             ;      4                     #                 Colors  already exist. Type another name  theme? A Controls collection for change look of gambas applications Advanced Advanced Configuration All files Background  Pictures Background: Bars and Buttons Cancel Choose a file Clone the Theme  Close Colorize Icon's  Controls Create a new Theme Default Delete Delete  Delete Selected Theme Duplicate selected tema Exit Font: Foreground: Free Selection Icons set according to Desktop Theme Image Files Imput Fields Invert Icons' Colors Maximize/Restore Minimize Name:  OK Picture to control size Properties Property Reset Reset to Desktop Theme's icons Revert Revert to default settings Round Control's Radius: Round Panel's Radius: Save Changes Save Changes Made Save Settings Select a color System Colors Text Color Theme  Theme: Themes are not installed on this computer. You must install or run the gbThemes program in order to use this Theme control. Themes editor for gambas projects Título Value Visualization Example gbThemes: Gambas3 Projects Themes Editor line:  Project-Id-Version: gbThemes-master 3.18.90
PO-Revision-Date: 2023-07-20 12:54 UTC
Last-Translator: gian <gian@gian>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
      Colori   esiste già. Digita un altro nome   tema? Una raccolta di controlli per cambiare l'aspetto delle applicazioni Gambas Avanzate Configurazione avanzata Tutti i files Immagini di sfondo Sfondo: Barre e pulsanti Annulla Scegli un file Clona il tema Chiudi Colora le icone Controlli Crea un nuovo tema Predefinito Eliminare Eliminare Elimina tema selezionato Duplica il tema selezionato Uscita Font: Primo piano: Selezione libera Icone impostate in base al tema del desktop File immagine Campi di input Inverti i colori delle icone Ingrandisci/Ripristina Minimizzare Nome: OK Immagine per controllare le dimensioni Proprietà Proprietà Ripristina Ripristina le icone del tema del desktop Ripristina Ripristina le impostazioni predefinite Raggio del controllo rotondo: Raggio del pannello rotondo: Salvare le modifiche Salva le modifiche apportate Salva le impostazioni Seleziona un colore Colori di sistema Colore del testo Tema Tema: I temi non sono installati su questo computer. È necessario installare o eseguire il programma gbThemes per utilizzare questo tema del controllo. Editor di temi per progetti Gambas Titolo Valore Esempio di visualizzazione gbThemes: editor di temi per progetti Gambas3 linea: 